package com.lapak.pelapakkite.models;

import static com.lapak.pelapakkite.json.fcm.FCMType.OTHER;

import java.io.Serializable;

/**
 * Created by Ourdevelops Team on 19/10/2019.
 */
public class Notif implements Serializable{
    private String token;
    private Data data;

    public Notif(final String token, final Data data) {
        this.token = token;
        this.data = data;
    }

    public static class Data {
        public int type = OTHER;
        public String title;
        public String message;
    }
}
