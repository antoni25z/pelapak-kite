package com.lapak.pelapakkite.utils.api.service;

import com.lapak.pelapakkite.json.fcm.ChatRequestJson;
import com.lapak.pelapakkite.json.fcm.DefaultResponseJson;
import com.lapak.pelapakkite.models.Notif;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface NotificationService {
    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body ChatRequestJson param);
    @POST("notificationapi/sendmobilenotification")
    Call<DefaultResponseJson> sendNotification(@Body Notif param);

}
