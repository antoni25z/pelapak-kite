package com.lapak.pelapakkite.utils.api.service;

import com.lapak.pelapakkite.json.ActiveCatRequestJson;
import com.lapak.pelapakkite.json.AddEditItemRequestJson;
import com.lapak.pelapakkite.json.AddEditKategoriRequestJson;
import com.lapak.pelapakkite.json.BankResponseJson;
import com.lapak.pelapakkite.json.CategoryRequestJson;
import com.lapak.pelapakkite.json.CategoryResponseJson;
import com.lapak.pelapakkite.json.ChangePassRequestJson;
import com.lapak.pelapakkite.json.DetailRequestJson;
import com.lapak.pelapakkite.json.DetailTransResponseJson;
import com.lapak.pelapakkite.json.EditMerchantRequestJson;
import com.lapak.pelapakkite.json.EditProfileRequestJson;
import com.lapak.pelapakkite.json.GetOnRequestJson;
import com.lapak.pelapakkite.json.GetServiceResponseJson;
import com.lapak.pelapakkite.json.HistoryRequestJson;
import com.lapak.pelapakkite.json.HistoryResponseJson;
import com.lapak.pelapakkite.json.HomeRequestJson;
import com.lapak.pelapakkite.json.HomeResponseJson;
import com.lapak.pelapakkite.json.ItemRequestJson;
import com.lapak.pelapakkite.json.ItemResponseJson;
import com.lapak.pelapakkite.json.LoginRequestJson;
import com.lapak.pelapakkite.json.LoginResponseJson;
import com.lapak.pelapakkite.json.NotificationResponseJson;
import com.lapak.pelapakkite.json.PrivacyRequestJson;
import com.lapak.pelapakkite.json.PrivacyResponseJson;
import com.lapak.pelapakkite.json.RegisterRequestJson;
import com.lapak.pelapakkite.json.RegisterResponseJson;
import com.lapak.pelapakkite.json.ResponseJson;
import com.lapak.pelapakkite.json.StripeRequestJson;
import com.lapak.pelapakkite.json.TopupRequestJson;
import com.lapak.pelapakkite.json.TopupResponseJson;
import com.lapak.pelapakkite.json.TransactionInfoRequestJson;
import com.lapak.pelapakkite.json.TransactionInfoResponseJson;
import com.lapak.pelapakkite.json.WalletRequestJson;
import com.lapak.pelapakkite.json.WalletResponseJson;
import com.lapak.pelapakkite.json.WithdrawRequestJson;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ourdevelops Team on 10/13/2019.
 */

public interface MerchantService {

    @GET("partnerapi/kategorimerchant")
    Call<GetServiceResponseJson> getFitur();

    @POST("customerapi/list_bank")
    Call<BankResponseJson> listbank(@Body WithdrawRequestJson param);

    @POST("partnerapi/kategorimerchantbyfitur")
    Call<GetServiceResponseJson> getKategori(@Body HistoryRequestJson param);

    @POST("partnerapi/onoff")
    Call<ResponseJson> turnon(@Body GetOnRequestJson param);

    @POST("partnerapi/login")
    Call<LoginResponseJson> login(@Body LoginRequestJson param);

    @POST("partnerapi/register_merchant")
    Call<RegisterResponseJson> register(@Body RegisterRequestJson param);

    @POST("partnerapi/forgot")
    Call<LoginResponseJson> forgot(@Body LoginRequestJson param);

    @POST("customerapi/privacy")
    Call<PrivacyResponseJson> privacy(@Body PrivacyRequestJson param);

    @POST("partnerapi/edit_profile")
    Call<LoginResponseJson> editprofile(@Body EditProfileRequestJson param);

    @POST("partnerapi/edit_merchant")
    Call<LoginResponseJson> editmerchant(@Body EditMerchantRequestJson param);

    @POST("partnerapi/home")
    Call<HomeResponseJson> home(@Body HomeRequestJson param);

    @POST("partnerapi/history")
    Call<HistoryResponseJson> history(@Body HistoryRequestJson param);

    @POST("partnerapi/detail_transaksi")
    Call<DetailTransResponseJson> detailtrans(@Body DetailRequestJson param);

    @POST("partnerapi/category")
    Call<CategoryResponseJson> category(@Body CategoryRequestJson param);

    @POST("partnerapi/item")
    Call<ItemResponseJson> itemlist(@Body ItemRequestJson param);

    @POST("partnerapi/active_kategori")
    Call<ResponseJson> activekategori(@Body ActiveCatRequestJson param);

    @POST("partnerapi/active_item")
    Call<ResponseJson> activeitem(@Body ActiveCatRequestJson param);

    @POST("partnerapi/add_kategori")
    Call<ResponseJson> addkategori(@Body AddEditKategoriRequestJson param);

    @POST("partnerapi/edit_kategori")
    Call<ResponseJson> editkategori(@Body AddEditKategoriRequestJson param);

    @POST("partnerapi/delete_kategori")
    Call<ResponseJson> deletekategori(@Body AddEditKategoriRequestJson param);

    @POST("partnerapi/add_item")
    Call<ResponseJson> additem(@Body AddEditItemRequestJson param);

    @POST("partnerapi/edit_item")
    Call<ResponseJson> edititem(@Body AddEditItemRequestJson param);

    @POST("partnerapi/delete_item")
    Call<ResponseJson> deleteitem(@Body AddEditItemRequestJson param);

    @POST("customerapi/topupstripe")
    Call<TopupResponseJson> topup(@Body TopupRequestJson param);

    @POST("partnerapi/withdraw")
    Call<ResponseJson> withdraw(@Body WithdrawRequestJson param);

    @POST("customerapi/wallet")
    Call<WalletResponseJson> wallet(@Body WalletRequestJson param);

    @POST("partnerapi/topuppaypal")
    Call<ResponseJson> topuppaypal(@Body WithdrawRequestJson param);

    @POST("partnerapi/changepass")
    Call<LoginResponseJson> changepass(@Body ChangePassRequestJson param);

    @POST("partnerapi/stripeaction")
    Call<ResponseJson> actionstripe(@Body StripeRequestJson param);

    @POST("partnerapi/intentstripe")
    Call<ResponseJson> intentstripe(@Body StripeRequestJson param);

    @POST("customerapi/createtransaction")
    Call<TransactionInfoResponseJson> createTransaction(@Body TransactionInfoRequestJson param);

    @GET("partnerapi/getmitranotification")
    Call<NotificationResponseJson> getMitraNotif();

}
