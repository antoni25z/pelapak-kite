package com.lapak.pelapakkite.utils.api.service;

import com.lapak.pelapakkite.json.SearchPlaceResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface GoogleService {

    @GET("place/textsearch/json")
    Call<SearchPlaceResponse> queryAutoComplete(@QueryMap Map<String, Object> params);

}
