package com.lapak.pelapakkite.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.databinding.ActivityNotificationBinding;
import com.lapak.pelapakkite.item.NotifAdapter;
import com.lapak.pelapakkite.json.NotificationResponseJson;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.utils.Log;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity {
    ActivityNotificationBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNotificationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        shimmershow();

        binding.notifRv.setLayoutManager(new LinearLayoutManager(this));

        User loginUser = BaseApp.getInstance(this).getLoginUser();
        MerchantService userService = ServiceGenerator.createService(MerchantService.class, loginUser.getNoTelepon(), loginUser.getPassword());

        userService.getMitraNotif().enqueue(new Callback<NotificationResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<NotificationResponseJson> call, @NonNull Response<NotificationResponseJson> response) {
                shimmertutup();
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).isSuccess()) {
                        if (response.body().getNotification().isEmpty()) {
                            binding.rlnodata.setVisibility(View.VISIBLE);
                            binding.notifRv.setVisibility(View.GONE);
                        } else {
                            binding.notifRv.setVisibility(View.VISIBLE);
                            binding.rlnodata.setVisibility(View.GONE);
                            binding.notifRv.setAdapter(new NotifAdapter(response.body().getNotification()));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<NotificationResponseJson> call, @NonNull Throwable t) {
                Log.d("2504", t.getMessage());
            }
        });

        binding.materialToolbar.setNavigationOnClickListener(view -> finish());

    }

    private void shimmershow() {
        binding.shimmerwallet.setVisibility(View.VISIBLE);
        binding.shimmerwallet.startShimmer();
    }

    private void shimmertutup() {
        binding.shimmerwallet.setVisibility(View.GONE);
        binding.shimmerwallet.stopShimmer();
    }
}