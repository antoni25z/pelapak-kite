package com.lapak.pelapakkite.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.content.ContextCompat;
import androidx.exifinterface.media.ExifInterface;

import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.json.AddEditItemRequestJson;
import com.lapak.pelapakkite.json.ResponseJson;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.utils.SettingPreference;
import com.lapak.pelapakkite.utils.Utility;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddmenuActivity extends AppCompatActivity {

    ImageView backbtn, menuimage;
    EditText namamenu, descmenu, hargamenu, hargapromo;
    SwitchCompat activepromo;
    Button submit, addimage;
    String active,idkategori;
    byte[] imageByteArray;
    Bitmap decoded;
    SettingPreference sp;

    ActivityResultLauncher<String> pickImage;
    ActivityResultLauncher<String> requestPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addmenu);

        backbtn = findViewById(R.id.back_btn);
        menuimage = findViewById(R.id.menuimage);
        namamenu = findViewById(R.id.namamenu);
        descmenu = findViewById(R.id.descmenu);
        hargamenu = findViewById(R.id.hargamenu);
        hargapromo = findViewById(R.id.hargapromo);
        activepromo = findViewById(R.id.activepromo);
        submit = findViewById(R.id.buttonsavemenu);
        addimage = findViewById(R.id.addimage);

        sp = new SettingPreference(this);

        Intent intent = getIntent();
        idkategori = intent.getStringExtra("idkategori");

        pickImage = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null) {
                scaleImage(result);
            }
        });

        requestPermission = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
            @Override
            public void onActivityResult(Boolean result) {
                if (result) {
                    pickImage.launch("image/*");
                }
            }
        });

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        hargamenu.addTextChangedListener(Utility.currencyTW(hargamenu,this));

        activepromo.setChecked(false);
        active = "0";
        hargapromo.setEnabled(false);
        activepromo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    hargapromo.setEnabled(true);
                    hargapromo.addTextChangedListener(Utility.currencyTW(hargapromo,AddmenuActivity.this));
                    active = "1";
                } else {
                    active = "0";
                    hargapromo.setText("");
                    hargapromo.setEnabled(false);
                }
            }
        });

        addimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (namamenu.getText().toString().isEmpty()){
                    Toast.makeText(AddmenuActivity.this, "Nama menu tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else if (descmenu.getText().toString().isEmpty()) {
                    Toast.makeText(AddmenuActivity.this, "Deskripsi tidak boleh kosong", Toast.LENGTH_SHORT).show();
                } else if (hargamenu.getText().toString().isEmpty()) {
                    Toast.makeText(AddmenuActivity.this, "Harga Tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else if (activepromo.isChecked() && hargapromo.getText().toString().isEmpty()) {
                    Toast.makeText(AddmenuActivity.this, "Harga Promo tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else if (activepromo.isChecked() && Long.parseLong(hargapromo.getText().toString().replace(sp.getSetting()[0],"").replace(".","")) > Long.parseLong(hargamenu.getText().toString().replace(sp.getSetting()[0],"").replace(".",""))) {
                    Toast.makeText(AddmenuActivity.this, "\n" +
                            "harga promo tidak boleh lebih tinggi dari harga dasar!", Toast.LENGTH_SHORT).show();
                } else if (decoded == null) {
                    Toast.makeText(AddmenuActivity.this, "Foto tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                } else {
                    addmenu();
                }
            }
        });

    }

    private void selectImage() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            if (check_ReadStoragepermission()) {
                pickImage.launch("image/*");
            } else {
                requestPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        } else {
            pickImage.launch("image/*");
        }

    }

    private boolean check_ReadStoragepermission() {
        return ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public String getPath(Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public String getStringImage(Bitmap bmp) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        imageByteArray = baos.toByteArray();
        return Base64.encodeToString(imageByteArray, Base64.DEFAULT);
    }

    private void scaleImage(Uri result) {
        String path = getPath(result);
        Matrix matrix = new Matrix();
        ExifInterface exif;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                exif = new ExifInterface(path);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            decoded = MediaStore.Images.Media.getBitmap(getContentResolver(), result);
            menuimage.setImageBitmap(decoded);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressLint("SetTextI18n")
    private void addmenu() {
        submit.setEnabled(false);
        submit.setText("Please Wait");
        submit.setBackground(getResources().getDrawable(R.drawable.button_round_3));
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        AddEditItemRequestJson request = new AddEditItemRequestJson();
        request.setNotelepon(loginUser.getNoTelepon());
        request.setIdmerchant(loginUser.getId_merchant());
        request.setNama(namamenu.getText().toString());
        request.setHarga(hargamenu.getText().toString().replace(".","").replace(sp.getSetting()[0],""));
        request.setHargapromo(hargapromo.getText().toString().replace(".","").replace(sp.getSetting()[0],""));
        request.setKategori(idkategori);
        request.setDeskripsi(descmenu.getText().toString());
        request.setStatus(active);
        request.setFoto(getStringImage(decoded));


        MerchantService service = ServiceGenerator.createService(MerchantService.class, loginUser.getEmail(), loginUser.getPassword());
        service.additem(request).enqueue(new Callback<ResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<ResponseJson> call,@NonNull Response<ResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        finish();

                    } else {
                        submit.setEnabled(true);
                        submit.setText("Save");
                        submit.setBackground(getResources().getDrawable(R.drawable.button_round_1));
                        Toast.makeText(AddmenuActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    submit.setEnabled(true);
                    submit.setText("Save");
                    submit.setBackground(getResources().getDrawable(R.drawable.button_round_1));
                    Toast.makeText(AddmenuActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseJson> call,@NonNull Throwable t) {
                t.printStackTrace();
                submit.setEnabled(true);
                submit.setText("Save");
                submit.setBackground(getResources().getDrawable(R.drawable.button_round_1));
                Toast.makeText(AddmenuActivity.this, "Error Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
