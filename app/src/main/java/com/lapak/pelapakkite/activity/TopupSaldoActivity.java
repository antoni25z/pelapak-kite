package com.lapak.pelapakkite.activity;

import static com.lapak.pelapakkite.constants.Constant.MIDTRANS_BASE_URL;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.json.TransactionInfoRequestJson;
import com.lapak.pelapakkite.json.TransactionInfoResponseJson;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.utils.SettingPreference;
import com.lapak.pelapakkite.utils.Utility;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BillingAddress;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.ShippingAddress;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.midtrans.sdk.uikit.api.model.CustomColorTheme;
import com.midtrans.sdk.uikit.api.model.TransactionResult;
import com.midtrans.sdk.uikit.external.UiKitApi;
import com.midtrans.sdk.uikit.internal.util.UiKitConstants;
import com.wensolution.wensxendit.PaymentMethod;
import com.wensolution.wensxendit.WensXendit;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TopupSaldoActivity extends AppCompatActivity {

    EditText nominal;
    ImageView text1, text2, text3, text4;
    RelativeLayout rlnotif, rlprogress;
    TextView textnotif;
    String disableback;
    LinearLayout banktransfer, creditcard, paypal,payumoney;
    private String paymentAmount;
    SettingPreference sp;
    ImageView backBtn;
    boolean debug;

    Button choosePm;

    ActivityResultLauncher<Intent> paymentLauncher;

    WensXendit wensXendit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);
        sp = new SettingPreference(this);

        nominal = findViewById(R.id.balance);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        rlnotif = findViewById(R.id.rlnotif);
        textnotif = findViewById(R.id.textnotif);
        rlprogress = findViewById(R.id.rlprogress);
        choosePm = findViewById(R.id.choose_pm_btn);

        wensXendit = new WensXendit(this);
        wensXendit.setXenditApiKey(sp.getSetting()[16]);
        wensXendit.setActiveMethods(new String[]{
                PaymentMethod.BRI,
                PaymentMethod.MANDIRI,
                PaymentMethod.BSI,
                PaymentMethod.PERMATA,
                PaymentMethod.BNI,
                PaymentMethod.BJB,
                PaymentMethod.DANA,
                PaymentMethod.OVO,
                PaymentMethod.SHOPEEPAY,
                PaymentMethod.LINKAJA,
                PaymentMethod.ASTRAPAY,
                PaymentMethod.QRIS,
        });

        nominal.addTextChangedListener(Utility.currencyTW(nominal,this));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        text1.setOnClickListener(v -> nominal.setText("20000"));

        text2.setOnClickListener(v -> nominal.setText("50000"));

        text3.setOnClickListener(v -> nominal.setText("100000"));

        text4.setOnClickListener(v -> nominal.setText("200000"));

        choosePm.setOnClickListener(view -> createTransaction());

        disableback = "false";
    }


    public void notif(String text) {
        rlnotif.setVisibility(View.VISIBLE);
        textnotif.setText(text);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                rlnotif.setVisibility(View.GONE);
            }
        }, 3000);
    }


    @Override
    public void onBackPressed() {
        if (!disableback.equals("true")) {
            finish();
        }
    }

    public void progressshow() {
        rlprogress.setVisibility(View.VISIBLE);
        disableback = "true";
    }

    public void progresshide() {
        rlprogress.setVisibility(View.GONE);
        disableback = "false";
    }

    public String convertAngka(String value) {
        return (((((value + "")
                .replaceAll(sp.getSetting()[0], ""))
                .replaceAll(" ", ""))
                .replaceAll(",", ""))
                .replaceAll("[Rp.]", ""));
    }

    private void createTransaction() {
        String referenceId = "pm-level-" + UUID.randomUUID();
        if (!nominal.getText().toString().isEmpty()) {
            progressshow();
            paymentAmount = nominal.getText().toString();
            final User user = BaseApp.getInstance(this).getLoginUser();
            TransactionInfoRequestJson request = new TransactionInfoRequestJson();
            request.setReferenceId(referenceId);
            request.setUserId(user.getId());
            request.setName(user.getNamamitra());
            request.setTypeUser("merchant");
            request.setAmount(convertAngka(paymentAmount));

            MerchantService service = ServiceGenerator.createService(MerchantService.class, user.getNoTelepon(), user.getPassword());
            service.createTransaction(request).enqueue(new Callback<>() {
                @Override
                public void onResponse(@NonNull Call<TransactionInfoResponseJson> call, @NonNull Response<TransactionInfoResponseJson> response) {
                    progresshide();
                    if (response.isSuccessful()) {
                        wensXendit.startPayment(
                                Long.parseLong(convertAngka(paymentAmount)),
                                Objects.requireNonNull(response.body()).getData().getReferenceId(),
                                response.body().getData().getName()
                        );
                    } else {
                        notif("error");
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TransactionInfoResponseJson> call, @NonNull Throwable t) {
                    progresshide();
                    notif(t.getMessage());
                }
            });
        } else {
            notif("Nominal Kosong !");
        }
    }
}
