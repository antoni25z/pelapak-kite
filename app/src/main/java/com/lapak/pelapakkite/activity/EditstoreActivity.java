package com.lapak.pelapakkite.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.content.res.ResourcesCompat;
import androidx.exifinterface.media.ExifInterface;


import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.constants.Constant;
import com.lapak.pelapakkite.json.EditMerchantRequestJson;
import com.lapak.pelapakkite.json.LoginResponseJson;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditstoreActivity extends AppCompatActivity {

    ImageView backbtn, bannermerchant;
    EditText namamerchant;
    Button submit, addimage;
    TextView merchantloc, opentime, closetime;
    private final int DESTINATION_ID = 1;
    String latitude, longitude;
    byte[] imageByteArray;
    Bitmap decoded;

    ActivityResultLauncher<Intent> startResultForMerchant;
    ActivityResultLauncher<String > pickImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editstore);

        backbtn = findViewById(R.id.back_btn);
        bannermerchant = findViewById(R.id.bannermerchant);
        namamerchant = findViewById(R.id.namamerchant);
        merchantloc = findViewById(R.id.merchantloc);
        opentime = findViewById(R.id.opentime);
        closetime = findViewById(R.id.closetime);
        submit = findViewById(R.id.buttonupdatemerchant);
        addimage = findViewById(R.id.addimage);

        User user = BaseApp.getInstance(this).getLoginUser();
        namamerchant.setText(user.getNamamerchant());
        merchantloc.setText(user.getAlamat_merchant());
        opentime.setText(user.getJam_buka());
        closetime.setText(user.getJam_tutup());
        latitude = user.getLatitude_merchant();
        longitude = user.getLongitude_merchant();

        Picasso.get()
                .load(Constant.IMAGESMERCHANT + user.getFoto_merchant())
                .placeholder(R.drawable.image_placeholder)
                .into(bannermerchant);


        startResultForMerchant = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getData() != null) {
                String addressset = result.getData().getStringExtra(PicklocationActivity.LOCATION_NAME);
                LatLng latLng = result.getData().getParcelableExtra(PicklocationActivity.LOCATION_LATLNG);
                merchantloc.setText(addressset);
                latitude = String.valueOf(Objects.requireNonNull(latLng).getLatitude());
                longitude = String.valueOf(latLng.getLongitude());
            }
        });



        pickImage = registerForActivityResult(new ActivityResultContracts.GetContent(), result -> {
            if (result != null) {
                scaleImage(result);
            }
        });

        opentime.setOnClickListener(v -> opentanggal());

        closetime.setOnClickListener(v -> closetanggal());

        merchantloc.setOnClickListener(v -> {
            Intent intent = new Intent(EditstoreActivity.this, PicklocationActivity.class);
            intent.putExtra(PicklocationActivity.FORM_VIEW_INDICATOR, DESTINATION_ID);
            startResultForMerchant.launch(intent);
        });

        addimage.setOnClickListener(v -> selectImage());

        backbtn.setOnClickListener(v -> finish());

        submit.setOnClickListener(v -> {
            if (namamerchant.getText().toString().isEmpty()) {
                Toast.makeText(EditstoreActivity.this, "Nama pelapak tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            } else if (merchantloc.getText().toString().isEmpty()) {
                Toast.makeText(EditstoreActivity.this, "Lokasi tidak boleh kosong!", Toast.LENGTH_SHORT).show();
            } else {
                editprofile();
            }
        });
    }

    private void closetanggal() {
        Calendar cur_calender = Calendar.getInstance();
        TimePickerDialog datePicker = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
            try {

                String timeString = hourOfDay + ":" + minute;
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.US);
                Date time = sdf.parse(timeString);

                sdf = new SimpleDateFormat("HH:mm", Locale.US);
                String formatedTime = sdf.format(Objects.requireNonNull(time));
                closetime.setText(formatedTime);

            } catch (Exception ignored) {
            }
        }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorgradient));
        datePicker.show(getSupportFragmentManager(), "Timepickerdialog");
    }

    private void opentanggal() {

        Calendar cur_calender = Calendar.getInstance();
        TimePickerDialog datePicker = TimePickerDialog.newInstance((view, hourOfDay, minute, second) -> {
            try {

                String timeString = hourOfDay + ":" + minute;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                Date time = sdf.parse(timeString);

                sdf = new SimpleDateFormat("HH:mm", Locale.US);
                String formatedTime = sdf.format(Objects.requireNonNull(time));
                opentime.setText(formatedTime);

            } catch (Exception ignored) {
            }
        }, cur_calender.get(Calendar.HOUR_OF_DAY), cur_calender.get(Calendar.MINUTE), true);
        datePicker.setThemeDark(false);
        datePicker.setAccentColor(getResources().getColor(R.color.colorgradient));
        datePicker.show(getSupportFragmentManager(), "Timepickerdialog");

    }

    private void selectImage() {
        pickImage.launch("image/*");
    }

    private void scaleImage(Uri result) {
        InputStream imageStream = null;
        try {
            imageStream = this.getContentResolver().openInputStream(Objects.requireNonNull(result));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        final Bitmap imagebitmap = BitmapFactory.decodeStream(imageStream);
        Bitmap scaleBitmap = Bitmap.createScaledBitmap(imagebitmap, (int) (imagebitmap.getWidth() * 0.1), (int) (imagebitmap.getHeight() * 0.1), true);

        String path = getPath(result);
        Matrix matrix = new Matrix();
        ExifInterface exif;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                exif = new ExifInterface(path);
                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), result);
            bannermerchant.setImageBitmap(bitmap);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            imageByteArray = baos.toByteArray();
            decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(baos.toByteArray()));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPath(Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = this.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        imageByteArray = baos.toByteArray();
        return Base64.encodeToString(imageByteArray, Base64.DEFAULT);
    }

    private void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(User.class);
        realm.copyToRealm(user);
        realm.commitTransaction();
        BaseApp.getInstance(EditstoreActivity.this).setLoginUser(user);
    }

    @SuppressLint("SetTextI18n")
    private void editprofile() {
        submit.setEnabled(false);
        submit.setText("Please Wait");
        submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_3, getTheme()));
        User loginUser = BaseApp.getInstance(this).getLoginUser();
        EditMerchantRequestJson request = new EditMerchantRequestJson();
        request.setNotelepon(loginUser.getNoTelepon());
        request.setAlamat(merchantloc.getText().toString());
        request.setLatitude_merchant(latitude);
        request.setLongitude_merchant(longitude);
        request.setJam_buka(opentime.getText().toString());
        request.setJam_tutup(closetime.getText().toString());
        request.setNamamerchant(namamerchant.getText().toString());
        if (imageByteArray != null) {
            request.setFoto_lama(loginUser.getFoto_merchant());
            request.setFoto_merchant(getStringImage(decoded));
        }


        MerchantService service = ServiceGenerator.createService(MerchantService.class, loginUser.getEmail(), loginUser.getPassword());
        service.editmerchant(request).enqueue(new Callback<LoginResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponseJson> call, @NonNull Response<LoginResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        User user = response.body().getData().get(0);
                        saveUser(user);
                        finish();

                    } else {
                        submit.setEnabled(true);
                        submit.setText("Save");
                        submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_1, getTheme()));
                        Toast.makeText(EditstoreActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    submit.setEnabled(true);
                    submit.setText("Save");
                    submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_1, getTheme()));
                    Toast.makeText(EditstoreActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoginResponseJson> call, @NonNull Throwable t) {
                t.printStackTrace();
                submit.setEnabled(true);
                submit.setText("Save");
                submit.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.button_round_1, getTheme()));
                Toast.makeText(EditstoreActivity.this, "Error Connection", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
