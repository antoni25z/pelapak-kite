package com.lapak.pelapakkite.activity.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.lapak.pelapakkite.json.SearchPlaceResponse;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.GoogleService;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceAutoCompleteViewModel extends ViewModel {
    MutableLiveData<List<SearchPlaceResponse.Result>> result = new MutableLiveData<>();

    GoogleService service = ServiceGenerator.createService(GoogleService.class, null, null);

    public void queryAutoComplete(Map<String, Object> params) {
        service.queryAutoComplete(params).enqueue(new Callback<>() {
            @Override
            public void onResponse(@NonNull Call<SearchPlaceResponse> call, @NonNull Response<SearchPlaceResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        result.setValue(response.body().getResults());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<SearchPlaceResponse> call, @NonNull Throwable t) {

            }
        });
    }

    public LiveData<List<SearchPlaceResponse.Result>> getPredictions() {
        return result;
    }
}
