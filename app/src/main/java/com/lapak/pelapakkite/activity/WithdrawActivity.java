package com.lapak.pelapakkite.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.databinding.ActivityWithdrawBinding;
import com.lapak.pelapakkite.item.BankItem;
import com.lapak.pelapakkite.item.BanksAdapter;
import com.lapak.pelapakkite.item.BanksClick;
import com.lapak.pelapakkite.json.BankResponseJson;
import com.lapak.pelapakkite.json.ResponseJson;
import com.lapak.pelapakkite.json.WithdrawRequestJson;
import com.lapak.pelapakkite.json.fcm.DefaultResponseJson;
import com.lapak.pelapakkite.models.Notif;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.utils.SettingPreference;
import com.lapak.pelapakkite.utils.Utility;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;
import com.lapak.pelapakkite.utils.api.service.NotificationService;
import com.wensolution.wensxendit.AvailableBankModel;
import com.wensolution.wensxendit.WensXendit;
import com.wensolution.wensxendit.apiservice.requestbody.ValidateNameRequestBody;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawActivity extends AppCompatActivity implements BanksClick {

    SettingPreference sp;
    private ActivityWithdrawBinding binding;

    BanksAdapter adapter;
    String bankName,bankCode, disburseCode;

    boolean disableback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityWithdrawBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        sp = new SettingPreference(this);

        progressshow();

        disableback = false;

        WensXendit wensXendit = new WensXendit(this);
        wensXendit.setIlumaApiKey(sp.getSetting()[17]);
        wensXendit.setXenditApiKey(sp.getSetting()[16]);

        binding.banksRv.setLayoutManager(new LinearLayoutManager(this));

        wensXendit.getAvailableBanks(availableBankModels -> {
            progresshide();
            adapter = new BanksAdapter((ArrayList<AvailableBankModel>) availableBankModels, this);
            binding.banksRv.setAdapter(adapter);
            return null;
        });

        binding.chooseBankEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isBlank()) {
                    binding.banksCard.setVisibility(View.GONE);
                } else {
                    binding.banksCard.setVisibility(View.VISIBLE);
                }
                adapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.nextBtn.setOnClickListener(view -> {
            progressshow();
            String bankNumber = Objects.requireNonNull(binding.withdrawNumberEdt.getText()).toString();

            ValidateNameRequestBody validateNameRequestBody = new ValidateNameRequestBody(
                    bankNumber,
                    bankCode
            );

            if (bankNumber.isBlank() || bankCode.isBlank() || Objects.requireNonNull(binding.chooseBankEdt.getText()).toString().isBlank()) {
                notif("Fill the field");
            } else {
                wensXendit.validateBankName(validateNameRequestBody, result -> {
                    progresshide();
                    if (result.getFound()) {
                        Intent intent = new Intent(this, ConfirmWithdrawActivity.class);
                        intent.putExtra("holder_name", result.getHolderName());
                        intent.putExtra("bank_name", result.getBankName());
                        intent.putExtra("bank_number", bankNumber);
                        intent.putExtra("disburse_code", disburseCode);
                        startActivity(intent);
                    } else {
                        notif(result.getMessage());
                    }
                    return null;
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (!disableback) {
            finish();
        }

    }

    public void notif(String text) {
        binding.rlnotif.setVisibility(View.VISIBLE);
        binding.textnotif.setText(text);

        new Handler().postDelayed(() -> binding.rlnotif.setVisibility(View.GONE), 3000);
    }


    public void progressshow() {
        binding.rlprogress.setVisibility(View.VISIBLE);
        disableback = true;
    }

    public void progresshide() {
        binding.rlprogress.setVisibility(View.GONE);
        disableback = true;
    }

    @Override
    public void onClick(AvailableBankModel availableBankModel) {
        bankName = availableBankModel.getName();
        bankCode = availableBankModel.getCodeIluma();
        disburseCode = availableBankModel.getCodeDisburse();

        binding.chooseBankEdt.setText(bankName);
        binding.banksCard.setVisibility(View.GONE);
    }


}
