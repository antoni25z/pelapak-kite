package com.lapak.pelapakkite.activity;

import static com.lapak.pelapakkite.constants.Constant.PRIVACY_URL;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.json.PrivacyRequestJson;
import com.lapak.pelapakkite.json.PrivacyResponseJson;
import com.lapak.pelapakkite.models.SettingsModel;
import com.lapak.pelapakkite.utils.NetworkUtils;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyActivity extends AppCompatActivity {

    WebView webView;
    ImageView backbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        webView = findViewById(R.id.webView);
        backbtn = findViewById(R.id.back_btn);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.loadUrl(PRIVACY_URL);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }
}

