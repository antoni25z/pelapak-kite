package com.lapak.pelapakkite.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.lapak.pelapakkite.activity.viewmodel.PlaceAutoCompleteViewModel;
import com.lapak.pelapakkite.constants.Constant;
import com.lapak.pelapakkite.databinding.ActivityPlaceAutoCompleteBinding;
import com.lapak.pelapakkite.item.ItemPlace;
import com.lapak.pelapakkite.item.ItemPlaceClick;
import com.lapak.pelapakkite.json.SearchPlaceResponse;

import java.util.HashMap;
import java.util.Map;

public class PlaceAutoCompleteActivity extends AppCompatActivity implements ItemPlaceClick {

    private ActivityPlaceAutoCompleteBinding binding;
    private PlaceAutoCompleteViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPlaceAutoCompleteBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(this).get(PlaceAutoCompleteViewModel.class);

        binding.placeRv.setLayoutManager(new LinearLayoutManager(this));

        viewModel.getPredictions().observe(this, predictions -> {
            if (!predictions.isEmpty()) {
                binding.placeRv.setAdapter(new ItemPlace(predictions, this));
            }
        });

        binding.searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isBlank()) {
                    binding.cardPlace.setVisibility(View.GONE);
                } else {
                    Map<String, Object> param = new HashMap<>();
                    param.put("key", Constant.GOOGLE_API_KEY);
                    param.put("query", charSequence.toString());
                    param.put("language", "in");
                    param.put("region", "ID");
                    viewModel.queryAutoComplete(param);
                    binding.cardPlace.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(SearchPlaceResponse.Result result) {
        Intent intent = new Intent();
        intent.putExtra(Constant.PICK_LOCATION_RESULT, result);
        setResult(RESULT_OK, intent);
        finish();
    }
}