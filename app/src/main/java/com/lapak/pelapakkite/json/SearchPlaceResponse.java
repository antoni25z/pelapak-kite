package com.lapak.pelapakkite.json;

import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public class SearchPlaceResponse {
    @SerializedName("html_attributions")
    private List<Object> htmlAttributions;
    private List<Result> results;
    private String status;

    public List<Object> getHTMLAttributions() { return htmlAttributions; }
    public void setHTMLAttributions(List<Object> value) { this.htmlAttributions = value; }

    public List<Result> getResults() { return results; }
    public void setResults(List<Result> value) { this.results = value; }

    public String getStatus() { return status; }
    public void setStatus(String value) { this.status = value; }

    public static class Result implements Serializable {
        @SerializedName("business_status")
        private BusinessStatus businessStatus;
        @SerializedName("formatted_address")
        private String formattedAddress;
        private Geometry geometry;
        private String icon;
        @SerializedName("icon_background_color")
        private IconBackgroundColor iconBackgroundColor;
        @SerializedName("icon_mask_base_uri")
        private String iconMaskBaseURI;
        private String name;
        @SerializedName("opening_hours")
        private OpeningHours openingHours;
        private List<Photo> photos;
        @SerializedName("place_id")
        private String placeID;
        @SerializedName("plus_code")
        private PlusCode plusCode;
        @SerializedName("price_level")
        private Long priceLevel;
        private Double rating;
        private String reference;
        private List<String> types;
        @SerializedName("user_ratings_total")
        private Long userRatingsTotal;

        public BusinessStatus getBusinessStatus() { return businessStatus; }
        public void setBusinessStatus(BusinessStatus value) { this.businessStatus = value; }

        public String getFormattedAddress() { return formattedAddress; }
        public void setFormattedAddress(String value) { this.formattedAddress = value; }

        public Geometry getGeometry() { return geometry; }
        public void setGeometry(Geometry value) { this.geometry = value; }

        public String getIcon() { return icon; }
        public void setIcon(String value) { this.icon = value; }

        public IconBackgroundColor getIconBackgroundColor() { return iconBackgroundColor; }
        public void setIconBackgroundColor(IconBackgroundColor value) { this.iconBackgroundColor = value; }

        public String getIconMaskBaseURI() { return iconMaskBaseURI; }
        public void setIconMaskBaseURI(String value) { this.iconMaskBaseURI = value; }

        public String getName() { return name; }
        public void setName(String value) { this.name = value; }

        public OpeningHours getOpeningHours() { return openingHours; }
        public void setOpeningHours(OpeningHours value) { this.openingHours = value; }

        public List<Photo> getPhotos() { return photos; }
        public void setPhotos(List<Photo> value) { this.photos = value; }

        public String getPlaceID() { return placeID; }
        public void setPlaceID(String value) { this.placeID = value; }

        public PlusCode getPlusCode() { return plusCode; }
        public void setPlusCode(PlusCode value) { this.plusCode = value; }

        public Long getPriceLevel() { return priceLevel; }
        public void setPriceLevel(Long value) { this.priceLevel = value; }

        public Double getRating() { return rating; }
        public void setRating(Double value) { this.rating = value; }

        public String getReference() { return reference; }
        public void setReference(String value) { this.reference = value; }

        public List<String> getTypes() { return types; }
        public void setTypes(List<String> value) { this.types = value; }

        public Long getUserRatingsTotal() { return userRatingsTotal; }
        public void setUserRatingsTotal(Long value) { this.userRatingsTotal = value; }

        public enum BusinessStatus implements Serializable {
            OPERATIONAL;

            public String toValue() {
                if (this == BusinessStatus.OPERATIONAL) {
                    return "OPERATIONAL";
                }
                return null;
            }

            public static BusinessStatus forValue(String value) throws IOException {
                if (value.equals("OPERATIONAL")) return OPERATIONAL;
                throw new IOException("Cannot deserialize BusinessStatus");
            }
        }

        public static class Geometry implements Serializable {
            private Location location;
            private Viewport viewport;

            public Location getLocation() { return location; }
            public void setLocation(Location value) { this.location = value; }

            public Viewport getViewport() { return viewport; }
            public void setViewport(Viewport value) { this.viewport = value; }
        }

        public static class Location implements Serializable {
            private Double lat;
            private Double lng;

            public Double getLat() { return lat; }
            public void setLat(Double value) { this.lat = value; }

            public Double getLng() { return lng; }
            public void setLng(Double value) { this.lng = value; }
        }
        public static class Viewport implements Serializable {
            private Location northeast;
            private Location southwest;

            public Location getNortheast() { return northeast; }
            public void setNortheast(Location value) { this.northeast = value; }

            public Location getSouthwest() { return southwest; }
            public void setSouthwest(Location value) { this.southwest = value; }
        }

        public enum IconBackgroundColor implements Serializable {
            FF9_E67, THE_13_B5_C7, THE_4_B96_F3, THE_7_B9_EB0;

            public String toValue() {
                return switch (this) {
                    case FF9_E67 -> "#FF9E67";
                    case THE_13_B5_C7 -> "#13B5C7";
                    case THE_4_B96_F3 -> "#4B96F3";
                    case THE_7_B9_EB0 -> "#7B9EB0";
                };
            }

            public static IconBackgroundColor forValue(String value) throws IOException {
                if (value.equals("#FF9E67")) return FF9_E67;
                if (value.equals("#13B5C7")) return THE_13_B5_C7;
                if (value.equals("#4B96F3")) return THE_4_B96_F3;
                if (value.equals("#7B9EB0")) return THE_7_B9_EB0;
                throw new IOException("Cannot deserialize IconBackgroundColor");
            }
        }

        public static class OpeningHours implements Serializable{
            @SerializedName("open_now")
            private Boolean openNow;

            public Boolean getOpenNow() { return openNow; }
            public void setOpenNow(Boolean value) { this.openNow = value; }
        }

        public static class Photo implements Serializable {
            private Long height;
            @SerializedName("html_attributions")
            private List<String> htmlAttributions;
            @SerializedName("photo_reference")
            private String photoReference;
            private Long width;

            public Long getHeight() { return height; }
            public void setHeight(Long value) { this.height = value; }

            public List<String> getHTMLAttributions() { return htmlAttributions; }
            public void setHTMLAttributions(List<String> value) { this.htmlAttributions = value; }

            public String getPhotoReference() { return photoReference; }
            public void setPhotoReference(String value) { this.photoReference = value; }

            public Long getWidth() { return width; }
            public void setWidth(Long value) { this.width = value; }
        }

        public static class PlusCode implements Serializable {
            @SerializedName("compound_code")
            private String compoundCode;
            @SerializedName("global_code")
            private String globalCode;

            public String getCompoundCode() { return compoundCode; }
            public void setCompoundCode(String value) { this.compoundCode = value; }

            public String getGlobalCode() { return globalCode; }
            public void setGlobalCode(String value) { this.globalCode = value; }
        }
    }
}


