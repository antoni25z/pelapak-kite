package com.lapak.pelapakkite.json.fcm;

import static com.lapak.pelapakkite.json.fcm.FCMType.CHAT;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatRequestJson {

    String token;
    Data data;

    public ChatRequestJson(final String token, final Data data) {
        this.token = token;
        this.data = data;
    }

    public static class Data {
        @Expose
        @SerializedName("type")
        int type = CHAT;

        @Expose
        @SerializedName("senderid")
        private String senderId;

        @Expose
        @SerializedName("receiverid")
        private String receiverId;

        @Expose
        @SerializedName("name")
        private String name;

        @Expose
        @SerializedName("pic")
        private String pic;

        @Expose
        @SerializedName("tokendriver")
        private String tokenDriver;

        @Expose
        @SerializedName("tokenuser")
        private String tokenUser;

        @Expose
        @SerializedName("message")
        private String message;

        public String getSenderId() {
            return this.senderId;
        }

        public void setSenderId(final String senderId) {
            this.senderId = senderId;
        }

        public String getReceiverId() {
            return this.receiverId;
        }

        public void setReceiverId(final String receiverId) {
            this.receiverId = receiverId;
        }

        public String getName() {
            return this.name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public String getPic() {
            return this.pic;
        }

        public void setPic(final String pic) {
            this.pic = pic;
        }

        public String getTokenDriver() {
            return this.tokenDriver;
        }

        public void setTokenDriver(final String tokenDriver) {
            this.tokenDriver = tokenDriver;
        }

        public String getTokenUser() {
            return this.tokenUser;
        }

        public void setTokenUser(final String tokenUser) {
            this.tokenUser = tokenUser;
        }

        public String getMessage() {
            return this.message;
        }

        public void setMessage(final String message) {
            this.message = message;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
