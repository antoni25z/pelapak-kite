package com.lapak.pelapakkite.item;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lapak.pelapakkite.databinding.ItemPlaceBinding;
import com.lapak.pelapakkite.json.SearchPlaceResponse;

import java.util.List;

public class ItemPlace extends RecyclerView.Adapter<ItemPlace.ViewHolder> {

    List<SearchPlaceResponse.Result> results;
    ItemPlaceClick itemPlaceClick;

    public ItemPlace(List<SearchPlaceResponse.Result> results, ItemPlaceClick itemPlaceClick) {
        this.results = results;
        this.itemPlaceClick = itemPlaceClick;
    }

    @NonNull
    @Override
    public ItemPlace.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPlaceBinding binding = ItemPlaceBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemPlace.ViewHolder holder, int position) {
        SearchPlaceResponse.Result result = results.get(position);
        holder.binding.titleOneTxt.setText(result.getName());
        holder.binding.titleTwoTxt.setText(result.getFormattedAddress());

        holder.itemView.setOnClickListener(view -> {
            itemPlaceClick.onClick(result);
        });
    }

    @Override
    public int getItemCount() {
        return results.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{

        ItemPlaceBinding binding;

        public ViewHolder(ItemPlaceBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
