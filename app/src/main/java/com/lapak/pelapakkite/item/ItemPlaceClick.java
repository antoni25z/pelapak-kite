package com.lapak.pelapakkite.item;

import com.lapak.pelapakkite.json.SearchPlaceResponse;

public interface ItemPlaceClick {
    void onClick(SearchPlaceResponse.Result result);
}
