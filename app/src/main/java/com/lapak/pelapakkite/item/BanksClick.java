package com.lapak.pelapakkite.item;

import com.wensolution.wensxendit.AvailableBankModel;

public interface BanksClick {
    void onClick(AvailableBankModel availableBankModel);
}
