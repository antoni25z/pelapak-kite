package com.lapak.pelapakkite.constants;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by ourdevelops Team on 10/23/2020.
 */

public class Constant {

    private static final String BASE_URL = "https://lapakkite.site/";
    public static final String CONNECTION = BASE_URL + "api/";
    public static final String IMAGESDRIVER = BASE_URL + "images/driverphoto/";
    public static final String IMAGESMERCHANT = BASE_URL + "images/merchant/";
    public static final String IMAGESBANK = BASE_URL + "images/bank/";
    public static final String IMAGESITEM = BASE_URL + "images/itemphoto/";
    public static final String IMAGESPELANGGAN = BASE_URL + "images/customer/";

    public static final String PRIVACY_URL = BASE_URL + "pelapak/privacypolicy.html";

    public static final String GOOGLE_BASE_URL = "https://maps.googleapis.com/maps/api/";

    public static String USERID = "uid";

    public static String PREF_NAME = "pref_name";

    public static int permission_camera_code = 786;
    public static int permission_write_data = 788;
    public static int permission_Read_data = 789;
    public static int permission_Recording_audio = 790;

    public static SimpleDateFormat df =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    public static String versionname = "1.0";

    public static final String GOOGLE_API_KEY = "AIzaSyAfgiNYxZX7Ak-3kDobVB3ZozJ8D6AhbBE";
    public static final String MIDTRANS_BASE_URL = CONNECTION + "customerapi/chargetoken/";

    public static final String PICK_LOCATION_RESULT = "PICK_LOCATION_RESULT";

    public static String UPDATED_DATE = "UPDATED_DATE";
    public static String PAYMENT_CODE = "PAYMENT_CODE";
    public static String REFERENCE_ID = "REFERENCE_ID";
    public static String TOTAL = "TOTAL";

    public static String CUSTOM_MAPBOX_STYLE = "mapbox://styles/irfan966/clh340ls400nq01qy5qrtckl4";
}
