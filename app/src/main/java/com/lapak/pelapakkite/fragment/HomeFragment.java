package com.lapak.pelapakkite.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.lapak.pelapakkite.R;
import com.lapak.pelapakkite.activity.IntroActivity;
import com.lapak.pelapakkite.activity.NotificationActivity;
import com.lapak.pelapakkite.activity.TopupSaldoActivity;
import com.lapak.pelapakkite.activity.WalletActivity;
import com.lapak.pelapakkite.activity.WithdrawActivity;
import com.lapak.pelapakkite.constants.BaseApp;
import com.lapak.pelapakkite.databinding.FragmentHomeBinding;
import com.lapak.pelapakkite.item.OrderItem;
import com.lapak.pelapakkite.json.GetOnRequestJson;
import com.lapak.pelapakkite.json.HomeRequestJson;
import com.lapak.pelapakkite.json.HomeResponseJson;
import com.lapak.pelapakkite.json.ResponseJson;
import com.lapak.pelapakkite.models.PayuModel;
import com.lapak.pelapakkite.models.TransMerchantModel;
import com.lapak.pelapakkite.models.User;
import com.lapak.pelapakkite.models.fcm.DriverResponse;
import com.lapak.pelapakkite.utils.SettingPreference;
import com.lapak.pelapakkite.utils.Utility;
import com.lapak.pelapakkite.utils.api.ServiceGenerator;
import com.lapak.pelapakkite.utils.api.service.MerchantService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;
import java.util.Objects;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    private OrderItem orderitem;

    private List<TransMerchantModel> order;
    private SettingPreference sp;
    private String status;

    FragmentHomeBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = new SettingPreference(requireContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = BaseApp.getInstance(requireContext()).getLoginUser();

        binding.namaPengguna.setText(user.getNamamitra());

        binding.orderanmasuk.setHasFixedSize(true);
        binding.orderanmasuk.setNestedScrollingEnabled(false);
        binding.orderanmasuk.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        getdata();

        binding.notifImg.setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), NotificationActivity.class);
            startActivity(intent);
        });

        binding.topup.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), TopupSaldoActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        });

        binding.withdraw.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), WithdrawActivity.class);
            i.putExtra("type","withdraw");
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        });

        binding.detail.setOnClickListener(v -> {
            Intent i = new Intent(getContext(), WalletActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        });
    }

    private void shimmershow() {
        binding.orderanmasuk.setVisibility(View.GONE);
        binding.shimmerhomme.startShimmer();
        binding.shimmerhomme.setVisibility(View.VISIBLE);

    }

    private void shimmertutup() {
        binding.shimmerhomme.stopShimmer();
        binding.shimmerhomme.setVisibility(View.GONE);
        binding.orderanmasuk.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    private void getdata() {
        shimmershow();
        if (order !=null) {
            order.clear();
        }
        binding.onoff.setSelected(false);
        binding.onoff.setText("wait...");
        binding.onoff.setEnabled(false);
        User loginUser = BaseApp.getInstance(requireContext()).getLoginUser();
        MerchantService merchantService = ServiceGenerator.createService(
                MerchantService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        HomeRequestJson param = new HomeRequestJson();
        param.setNotelepon(loginUser.getNoTelepon());
        param.setIdmerchant(loginUser.getId_merchant());
        param.setIdmitra(loginUser.getId());
        merchantService.home(param).enqueue(new Callback<HomeResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<HomeResponseJson> call, @NonNull Response<HomeResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equalsIgnoreCase("success")) {
                        PayuModel payu = response.body().getPayu().get(0);
                        sp.updateCurrency(response.body().getCurrency());
                        sp.updateabout(response.body().getAboutus());
                        sp.updateemail(response.body().getEmail());
                        sp.updatephone(response.body().getPhone());
                        sp.updateweb(response.body().getWebsite());
                        sp.updatePaypal(response.body().getPaypalkey());
                        sp.updatepaypalmode(response.body().getPaypalmode());
                        sp.updatepaypalactive(response.body().getPaypalactive());
                        sp.updatestripeactive(response.body().getStripeactive());
                        sp.updatecurrencytext(response.body().getCurrencytext());
                        sp.updatePayudebug(payu.getPayudebug());
                        sp.updatePayumerchantid(payu.getPayuid());
                        sp.updatePayusalt(payu.getPayusalt());
                        sp.updatePayumerchantkey(payu.getPayukey());
                        sp.updatePayuActive(payu.getActive());
                        sp.updateXenditKey(response.body().getXenditApiKey());
                        sp.updateIlumaKey(response.body().getIlumaApiKey());
                        sp.updateStripepublish(response.body().getStripePublished());
                        order = response.body().getData();
                        shimmertutup();
                        Utility.currencyTXT(binding.balance,response.body().getSaldo(),getContext());
                        orderitem = new OrderItem(getContext(), order, R.layout.item_order);
                        binding.orderanmasuk.setAdapter(orderitem);
                        if (response.body().getData().isEmpty()) {
                            binding.orderanmasuk.setVisibility(View.GONE);
                            binding.rlnodata.setVisibility(View.VISIBLE);
                        } else {
                            binding.orderanmasuk.setVisibility(View.VISIBLE);
                            binding.rlnodata.setVisibility(View.GONE);
                        }
                        User user = response.body().getUser().get(0);
                        binding.onoff.setEnabled(true);
                        if (user.getStatus_merchant().equals("1")) {
                            binding.onoff.setSelected(true);
                            binding.onoff.setText("On");
                        } else {
                            binding.onoff.setSelected(false);
                            binding.onoff.setText("Off");
                        }

                        status = user.getStatus_merchant();
                        binding.onoff.setOnClickListener(v -> getturnon(status));
                        saveUser(user);
                        if (HomeFragment.this.getActivity() != null) {
                            Realm realm = BaseApp.getInstance(HomeFragment.this.getActivity()).getRealmInstance();
                            User loginUser = BaseApp.getInstance(HomeFragment.this.getActivity()).getLoginUser();
                            realm.beginTransaction();
                            loginUser.setWalletSaldo(Long.parseLong(response.body().getSaldo()));
                            realm.commitTransaction();
                        }
                    } else {
                        Realm realm = BaseApp.getInstance(requireContext()).getRealmInstance();
                        realm.beginTransaction();
                        realm.delete(User.class);
                        realm.commitTransaction();
                        BaseApp.getInstance(requireContext()).setLoginUser(null);
                        startActivity(new Intent(requireContext(), IntroActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        requireActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<HomeResponseJson> call, @NonNull Throwable t) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getturnon(String statuson) {
        binding.onoff.setSelected(false);
        binding.onoff.setText("Wait...");
        binding.onoff.setEnabled(false);
        User loginUser = BaseApp.getInstance(requireContext()).getLoginUser();
        MerchantService userService = ServiceGenerator.createService(
                MerchantService.class, loginUser.getNoTelepon(), loginUser.getPassword());
        GetOnRequestJson param = new GetOnRequestJson();
        param.setId(loginUser.getId_merchant());
        param.setToken(loginUser.getToken_merchant());
        if (statuson.equals("1")) {
            param.setTurn("2");
        } else {
            param.setTurn("1");
        }

        userService.turnon(param).enqueue(new Callback<ResponseJson>() {
            @Override
            public void onResponse(@NonNull Call<ResponseJson> call, @NonNull Response<ResponseJson> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMessage().equals("success")) {
                        binding.onoff.setEnabled(true);
                        status = response.body().getData();
                        if (response.body().getData().equals("1")) {
                            binding.onoff.setSelected(true);
                            binding.onoff.setText("On");
                        } else if (response.body().getData().equals("2")) {
                            binding.onoff.setSelected(false);
                            binding.onoff.setText("Off");
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseJson> call, @NonNull Throwable t) {

            }
        });
    }

    private void saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(User.class);
        realm.copyToRealm(user);
        realm.commitTransaction();
        BaseApp.getInstance(requireContext()).setLoginUser(user);
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final DriverResponse response) {
        if (response.getResponse().equals("2") || response.getResponse().equals("3") || response.getResponse().equals("4") || response.getResponse().equals("5")) {
            getdata();
        }

    }


}
